import styled from 'styled-components'

export const LoginWrapper = styled.div`
    width: 100%;
    height: 100vh;
    position: relative;
    background-image: url('https://unsircle.com/wp-content/uploads/2020/02/BG-blurred.jpg');
`

export const LoginBox = styled.div`
    width: 100%;
    background: white;
    height: 400px;
    max-width: 400px;
    padding: 15px;
    border-radius: 5px;
    box-shadow: 0px 0px 10px 1px rgba(9,0,0,0.1);
    margin: auto;
    position: absolute;
    left: 0;
    right: 0;
    bottom: 0;
    top: 0;
`

export const LoginIcon = styled.img`
    width: 60%;
    margin: 15px auto;
    display: block;
`