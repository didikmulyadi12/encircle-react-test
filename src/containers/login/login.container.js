import React, { useState, useEffect } from 'react'
import { useHistory } from "react-router-dom";
import {
    connect,
    useDispatch,
    useSelector
} from 'react-redux'
import { setIsLoading } from '../../redux/actions';

import { LoginWrapper, LoginBox, LoginIcon } from './login.style'
import { Button, Form, FormGroup, Label, Input, Alert } from 'reactstrap';
import { setUserLogin } from '../../redux/actions';

const dataUser = {
    email : 'test@unsircle.com',
    password : '12345'
}

const Login = () => {
    const history = useHistory();
    const dispatch = useDispatch()
    const userLogin = useSelector(state => state.userLogin || null)

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [isSubmit, setIsSubmit] = useState(false)
    const [loginMessage, setLoginMessage] = useState('')

    const onLogin = async(e) => {
        e.preventDefault()

        if(isSubmit === false) {
            setIsSubmit(true)
            await dispatch(setIsLoading(true))

            if(email === 'test@unsircle.com' && password === '12345') {
                setTimeout(async() => {
                    await dispatch(setUserLogin(dataUser))
                    history.push("/");      
                }, 2000)
            }else{
                setLoginMessage('Email atau password salah')
                setIsSubmit(false)
                dispatch(setIsLoading(false))
            }
        }

    }

    useEffect(() => {
        userLogin && history.push("/");
    })

    return (
        <LoginWrapper>
            <LoginBox>
                <LoginIcon src={'https://unsircle.com/wp-content/uploads/2020/01/Unsircle-logo-png-200x56.png'} />
                <Form onSubmit={onLogin}>
                    {
                        loginMessage !== '' && (
                            <Alert color="danger" isOpen={true} toggle={() => setLoginMessage('')}>
                                {loginMessage}
                            </Alert>
                        )
                    }
                    <FormGroup>
                        <Label for="exampleEmail">Email</Label>
                        <Input type="email" name="email" id="exampleEmail" required  placeholder="with a placeholder" value={email} onChange={({ target }) => setEmail(target.value)}/>
                    </FormGroup>
                    <FormGroup>
                        <Label for="examplePassword">Password</Label>
                        <Input type="password" name="password" id="examplePassword" required placeholder="password placeholder" value={password} onChange={({ target }) => setPassword(target.value)}/>
                    </FormGroup>
                    <Button className="float-right" type="submit">Submit</Button>
                </Form>
            </LoginBox>
        </LoginWrapper>
    )
}

export default connect()(Login)