import React, { useEffect } from 'react'
import { useHistory } from "react-router-dom";
import {
    connect,
    useSelector,
    useDispatch
} from 'react-redux'


import { HomeWrapper } from './home.style'
import { Button } from 'reactstrap'
import { setUserLogin } from '../../redux/actions'

const Home = () => {
    const userLogin = useSelector(state => state.userLogin || null)
    const dispatch = useDispatch()
    const history = useHistory();

    useEffect(() => {
        !userLogin && history.push("/login");
    }, [])

    const logout = async() => {
        await dispatch(setUserLogin(null))
        history.push("/login");
    }

    return (
        <HomeWrapper>
            <div>Selamat Datang {userLogin ? userLogin.email : ''}</div>
            <Button className="float-right" onClick={logout}>Logout</Button>
        </HomeWrapper>
    )
}

export default connect()(Home)