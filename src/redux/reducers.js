import { setUserLoginName, setLoadingName } from "./names";

const reducer = (state = {}, { type, payload }) => {
    switch (type) {
       case setUserLoginName:
        return { ...state, userLogin: payload, isLoading: false };
        case setLoadingName:
        return { ...state, isLoading: payload };
       default:
         return state;
     }
};

export default reducer;