import { setUserLoginName, setLoadingName } from "./names";

export const setUserLogin = (payload) => ({
    type: setUserLoginName,
    payload
})

export const setIsLoading = (payload) => ({
    type: setLoadingName,
    payload
})