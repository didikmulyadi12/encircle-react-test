import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import './App.css'
import Login from './containers/login';
import Home from './containers/home';
import Loader from './components/loader';

import {
  connect,
  useSelector
} from 'react-redux'

function App() {
  const isLoading = useSelector(state => state.isLoading)
  const userLogin = useSelector(state => state.userLogin)

  return (
    <Router>
      { isLoading && <Loader /> }
      <Switch>
          <Route path="/login">
            <Login />
          </Route>
          <Route path="/" exact>
            <Home />
          </Route>
        </Switch>
      
    </Router>
  );
}

export default connect()(App);
